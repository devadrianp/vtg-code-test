// <!-- === script for mobile menu ===== -->

$(".mobile_menu").click(function() {
    $(".header_links").addClass("show_links");
});

$("#closeMenu").click(function() {
    $(".header_links").removeClass("show_links");
});



// form tabs
$('.tabs-nav a').on('click', function(event) {
    event.preventDefault();

    $('.tab-active').removeClass('tab-active');
    $(this).parent().addClass('tab-active');
    $('.tabs-stage .form_content').hide();
    $($(this).attr('href')).show();
});

$('.tabs-nav a:first').trigger('click');


//  for model

$(".close_popup").click(function() {
    $(".log_popup").fadeOut("300");
});

$(".show_popup").click(function() {
    $(".log_popup").fadeIn("300");
});